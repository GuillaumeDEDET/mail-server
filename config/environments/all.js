'use strict';

const _             = require('lodash');
const env           = require('./' + (process.env.NODE_ENV || 'development'));

const all           = {
    log : {
        showRouteAtStart : true
    },
    connections : {
        api : {
            host    : '0.0.0.0',
            port    : process.env.PORT || 8889,
            labels  : [ 'api' ]
        }
    },
    mail: {
        service: 'gmail',
        auth: {
            user: 'ladressedunodeetplus@gmail.com',
            pass: 'lenodejadoreca'
        }
    }
};

module.exports = _.merge(all, env);
