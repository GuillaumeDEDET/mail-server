'use strict';
const nodemailer = require('nodemailer');
const Mailgen = require('mailgen');

module.exports.root = (request, response) => {
    response(null,  {
        result : 'vous êtes connectés'
    });
};

const mailGenerator = new Mailgen({
    theme: 'salted',
    product: {
        name: 'Project API Hapi NodeJS',
        link: 'https://github.com/GuillaumeDEDET',
        logo: 'https://avatars3.githubusercontent.com/u/16645543?v=3&u=70a6c7e4df1139ab54aa41ba053dcb35854ae90a&s=400'
    }
});


module.exports.creationMail = (request, reply) => {
    let smtpTransport = nodemailer.createTransport(request.server.app.envs.mail);

    let email = {
        body: {
            name: request.payload.name,
            intro: 'We confirm the creation of your account : <br\>' +
            'Your login : ' + request.payload.login + '<br\>'+
            'Your password : ' + request.payload.password,
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        }
    };

    let mailOptions = {
        from: '"Hapi Project" <guillaume.dedet@etu.unilim.fr>',
        to: request.payload.emailAddress,
        subject: request.payload.name + ': Creation Confirm',
        html: mailGenerator.generate(email),
        text: mailGenerator.generatePlaintext(email),
    };

    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            reply.boom(500, err, "Email Error");
            return;
        }

        reply(null, { msg : 'Mail sended' });
    });
};

module.exports.editionMail = (request, reply) => {

    let smtpTransport = nodemailer.createTransport(request.server.app.envs.mail);

    let email = {
        body: {
            name: request.payload.name,
            intro: 'We confirm the modification of your connection informations <br\>',
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        }
    };

    let mailOptions = {
        from: '"Hapi Project" <guillaume.dedet@etu.unilim.fr>',
        to: request.payload.emailAddress,
        subject: request.payload.name + ': Edition Confirm',
        html: mailGenerator.generate(email),
        text: mailGenerator.generatePlaintext(email),
    };

    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            reply.boom(500, err, "Email Error");
            return;
        }

        reply(null, { msg : 'Mail sended' });
    });
};

module.exports.resetPasswordMail = (request, reply) => {

    let smtpTransport = nodemailer.createTransport(request.server.app.envs.mail);

    let email = {
        body: {
            name: request.payload.name,
            intro: 'Here are your new credentials :<br\>' +
            'Your login : ' + request.payload.login + '<br\>'+
            'Your password : ' + request.payload.password,
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.'
        }
    };

    let mailOptions = {
        from: '"Hapi Project" <guillaume.dedet@etu.unilim.fr>',
        to: request.payload.emailAddress,
        subject: request.payload.name + ': Password reset',
        html: mailGenerator.generate(email),
        text: mailGenerator.generatePlaintext(email),
    };

    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            reply.boom(500, err, "Email Error");
            return;
        }

        reply(null, { msg : 'Mail sended' });
    });
};