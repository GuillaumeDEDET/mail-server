'use strict';

const handler = require('../handlers/default');
const Joi = require('joi');

exports.register = (server, options, next) => {
    server.route([
        {
            method : 'POST',
            path   : '/mail/creation',
            config : {
                description : 'Mail de création d\'un utilsateur',
                notes       : 'Route d\'envoi de mail de confirmation de création d\'utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-io': 'mail-creation'
                },
                validate: {
                    payload: {
                        emailAddress: Joi.string().required(),
                        name: Joi.string().required(),
                        login: Joi.string().required(),
                        password: Joi.string().required()
                    }
                },
                handler     : handler.creationMail,
            }
        },
        {
            method : 'POST',
            path   : '/mail/edition',
            config : {
                description : 'Mail d\'édition d\'un utilsateur',
                notes       : 'Route d\'envoi de mail de confirmation d\'édition d\'utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-io': 'mail-edition',
                },
                handler     : handler.editionMail,
            }
        },
        {
            method : 'POST',
            path   : '/mail/reset',
            config : {
                description : 'Mail de reset de mot de passe',
                notes       : 'Route d\'envoi de mail avec le nouveau mot de passe',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-io': 'mail-reset',
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                validate: {
                    payload: {
                        emailAddress: Joi.string().required(),
                        name: Joi.string().required(),
                        login: Joi.string().required(),
                        password: Joi.string().required()
                    }
                },
                handler     : handler.resetPasswordMail,
            }
        }
    ]);
    next();
};

exports.register.attributes = {
    name : 'default-routes'
};